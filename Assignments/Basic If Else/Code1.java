class Code1 {
    public static void main(String[] args) {
        int num1 = 5;
        int num2 = -9;

        if (num1>0){
            System.out.println(num1 + " is a positive number");
        }
        else if (num1<0){
            System.out.println(num1 + " is a negative number");
        }
        else{
            System.out.println(num1 + " is a zero");
        }

        if (num2>0){
            System.out.println(num2 + " is a positive number");
        }
        else if (num2<0){
            System.out.println(num2 + " is a negative number");
        }
        else{
            System.out.println(num2 + " is a zero");
        }
    }
}
