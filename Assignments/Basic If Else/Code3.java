class Code3 {
    public static void main(String[] args) {
        int num1 = 10;

        if (num1%2==0){
            if (num1<10) {
                System.out.println(num1 + " is a even number and less than 10");
            }
            else if (num1>10){
                System.out.println(num1 + " is a even number and greater than 10");
            }
            else{
                System.out.println(num1 + " is a even number and equal to 10");
            }
        }
        else {
            if (num1<10) {
                System.out.println(num1 + " is a odd number and less than 10");
            }
            else if (num1>10){
                System.out.println(num1 + " is a odd number and greater than 10");
            }
            else{
                System.out.println(num1 + " is a odd number and equal to 10");
            }
        }
    }
}
