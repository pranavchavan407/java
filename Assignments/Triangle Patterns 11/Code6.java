/*
Enter the number of rows: 3

3 3 3
2 2
1

Enter the number of rows: 4

4 4 4 4
3 3 3
2 2
1
 
*/
import java.util.*;
class Code6{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");
        int rows = sc.nextInt();
        sc.close();

        for (int i = rows; i >=1; i--) {
            for (int j = 1; j<=i; j++) {
                System.out.print(i+" ");
            }
            System.out.println();
        }
    }
}