/*
Enter the number of rows: 3

3
3 6
3 6 9

Enter the number of rows: 4

4
4 8
4 8 12
4 8 12 16
 
*/
import java.util.*;
class Code4{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");
        int rows = sc.nextInt();
        sc.close();

        for (int i = 1; i <=rows; i++) {
            int num = rows;
            for (int j = 1; j<= i; j++) {
                System.out.print(num+" ");
                num+=rows;
            }
            System.out.println();
        }
    }
}