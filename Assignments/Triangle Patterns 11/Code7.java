/*
Enter the number of rows: 3

1 2 3
1 2
1

Enter the number of rows: 4

1 2 3 4
1 2 3
1 2
1
 
*/
import java.util.*;
class Code7{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");
        int rows = sc.nextInt();
        sc.close();

        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j<=rows+1-i; j++) {
                System.out.print(j+" ");
            }
            System.out.println();
        }
    }
}