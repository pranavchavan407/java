/*
Enter the number of rows: 3

A 66 C
66 C
C

Enter the number of rows: 4

65 B 67 D
B 67 D
67 D
D
 
*/
import java.util.*;
class Code10{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");
        int rows = sc.nextInt();
        sc.close();

        for (int i = 1; i <= rows; i++) {
            int num = 65+i-1;
            
            for (int j = i; j <= rows; j++) {
                if (rows%2==0) {
                    if (num%2==0) {
                        System.out.print((char) num+" ");
                        num++;
                    }
                    else{
                        System.out.print(num+" ");
                        num++;
                    }    
                }
                else{
                    if (num%2==1) {
                        System.out.print((char) num+" ");
                        num++;
                    }
                    else{
                        System.out.print(num+" ");
                        num++;
                    }
                }
            }
            System.out.println();
        }
    }
}