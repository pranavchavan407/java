/*
Enter the number of rows: 5

E D C B A
E D C B
E D C
E D
E

Enter the number of rows: 4

D C B A
D C B
D C
D
 
*/
import java.util.*;
class Code9{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");
        int rows = sc.nextInt();
        sc.close();

        for (int i = rows; i >=1; i--) {
            char ch =(char)(64+rows);
            for (int j = 1; j<=i; j++) {
                System.out.print(ch+" ");
                ch--;
            }
            System.out.println();
        }
    }
}