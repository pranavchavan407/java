/*
Enter the number of rows: 3

1
2 4
3 6 9

Enter the number of rows: 4

1
2 4
3 6 9
4 8 12 16
 
*/
import java.util.*;
class Code5{
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");
        int rows = sc.nextInt();
        sc.close();

        for (int i = 1; i <=rows; i++) {
            int num = i;
            for (int j = 1; j<= i; j++) {
                System.out.print(num+" ");
                num+=i;
            }
            System.out.println();
        }
    }
}