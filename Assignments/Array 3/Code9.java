import java.util.Scanner;
class Code9{
	public static void main (String[] args){
	    Scanner sc = new Scanner (System.in);
	    System.out.print("Enter size : ");
	    int size=sc.nextInt();
	    int arr[]=new int[size];
    
	    System.out.println("Enter the elements of the array: ");
	    for(int i=0; i<arr.length; i++)
	    {
	        arr[i] = sc.nextInt();
	    }
    
	    System.out.println("Prime numbers in the array are : ");
	    for(int i=0; i<arr.length; i++){
	        int count=0;
		for (int j=2; j<arr[i]; j++){
            		if(arr[i]%j==0){
	                count++;
	                break;
	            }
		}
		if(count==0){
	            System.out.println(arr[i] +"\t");
		}
	    }
	}
}
