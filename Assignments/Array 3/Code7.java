import java.util.*;
class Code7{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.print("Enter size : ");
                int size=sc.nextInt();
                int arr[]=new int[size];

                System.out.println("Enter element : ");
                for(int i=0; i<arr.length; i++){
                        arr[i]=sc.nextInt();
                }
		
		System.out.println("Output : ");
                for(int i=0; i<arr.length; i++){
                        if(size%2==0){
                                if(arr[i]%2==0){
					System.out.print(arr[i]+"\t");
				}
                        }else{
				if(arr[i]%2==1){
					System.out.print(arr[i]+"\t");
				}
			}
                }
                System.out.println();
        }
}
