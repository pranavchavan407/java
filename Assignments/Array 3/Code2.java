/*
2.Write a program to find the first occurrences of a specific number in an array. Print the index of a first occurrence.

Example 1:
Input: 
Specific number : 5
Output: num 5 first occurred at index : 1

Example 2:
Input:
Specific number : 11
Output: num 11 not found in array.
*/
import java.util.*;
class Code2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[]{1,5,9,8,7,6};

        boolean found = false;

        System.out.print("Enter the specific Number : ");

        int num = sc.nextInt();
        
        int index = -1;

        for(int i=0;i<arr.length;i++){
            if (arr[i]== num) {
                found = true;
                index = i;
                break;
            }
        }
        
        if (found) {
            System.out.println("Num "+num+" first occured at index "+index);
        }
        else{
            System.out.println("Num "+num+" not found in array");
        }
        sc.close();
    }    
}
