/*
3.Write a program to find the number of occurrences of a specific number in an array. Print the count of occurrences.

Example 1:
Input: 
Specific number : 2
Output: Number 2 occurred 3 times in an array.

Example 2:
Input:
Specific number : 11
Output: num 11 not found in array.
*/
import java.util.*;
class Code3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[]{2,5,2,7,8,9,2};

        System.out.print("Enter the specific Number : ");

        int num = sc.nextInt();
        
        boolean notFound = true;
        int count = 0;

        for(int i=0;i<arr.length;i++){
            if (arr[i]== num) {
                notFound = false;
                count+=1;
            }
        }
        
        if (notFound) {
            System.out.println("Num "+num+" not found in array");
        }
        else{
            System.out.println("Number "+num+" occured "+count+" times in an array");
        }
        sc.close();
    }    
}
