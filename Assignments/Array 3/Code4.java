/*
4.Write a program to convert all even numbers into O and odd numbers into 1 in a given array.

input :
1 2 3 12 15 6 7 10 9 

output :
1 0 1 0 1 0 1 0 1
*/

class Code4 {
    public static void main(String[] args) {
        int arr[] = new int[]{1,2,3,12,15,6,7,10,9};

        int newArr[] = new int[arr.length];

        for(int i=0;i<arr.length;i++){
            if(arr[i]%2==0){
                newArr[i] = 0;
            }
            else{
                newArr[i] = 1;
            }
        }
        
        
        for(int i=0;i<arr.length;i++){
            System.out.print(newArr[i]+" ");
        }
    }    
}
