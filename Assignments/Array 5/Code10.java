import java.util.*;
class Code10{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }

        for (int i = 0; i < arr.length; i++) {
            int num = arr[i];
            long factorial = 1;
            for(int j=2;j<=num;j++){
                factorial*=j;
            }
            System.out.print(factorial+",");
        }
        sc.close();
    }    
}
