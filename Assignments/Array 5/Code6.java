/*
6. WAP to find the first prime number in an array.

Input:
Enter the size of the array:6
Enter the elements of the array:
10
22
3
31
50
3
Output:
First prime number found at index 2
*/

import java.util.*;
class Code6{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the size of an array : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        for (int i = 0; i < arr.length; i++) {
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }

        
        for (int i = 0; i < arr.length; i++) {
            boolean prime = true;
            if (arr[i]<=1) {
                prime=false;
            }
            else{
                for(int j=2;j<=arr[i]/2;j++){
                    if (arr[i]%j==0) {
                        prime=false;
                        break;
                    }
                }    
            }
            if (prime) {
                System.out.println("First prime number found at index "+i);
                break;
            }
            else{
                System.out.println("NO prime number found in the array");
            }
        }
        sc.close();
    }
}