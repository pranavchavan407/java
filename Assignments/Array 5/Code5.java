/*
5. WAP to print the count of digits in elements of an array.

Input:
Enter the size of the array:4
Enter the elements of the array:
1
225
32
356

Output:
1,3,2,3
*/
import java.util.*;
class Code5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        int newarr[] = new int[size];
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        for(int i=0;i<size;i++){
            int count = 0;
            while(arr[i]!=0){
                arr[i] /= 10;
                count++;
            }
            
            newarr[i] = count;
        }
        
        for(int i=0;i<size;i++){
            System.out.print(newarr[i]+",");
        }
        
        sc.close();
    }    
}
