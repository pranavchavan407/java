import java.util.*;
class Code9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the Number : ");
        int number = sc.nextInt();
        String num = Integer.toString(number);

        int arr[] = new int[num.length()];
        
        for (int i = 0; i < arr.length; i++) {
            int digit = Character.getNumericValue(num.charAt(i));
            arr[i] = digit + 1;
        }

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+",");
        }
        sc.close();
    }    
}
