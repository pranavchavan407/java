/*
4. WAP to check the first duplicate element in an array and retum its index.

Input:
Enter the size of the array:6
Enter the elements of the array:
1
2
3
3
2
5

Output:
First duplicate element present at index 1
*/
import java.util.*;
class Code4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        int index = -1;

        for(int i=0;i<size;i++){
            for(int j=i+1;j<size;j++){
                if(arr[i]==arr[j]){
                    index = i;
                    break;
                }
            }
            if (index != -1) {
                break;    
            }
        }
        
        if (index != -1) {
            System.out.println("The first duplicate element is at index: " + index);
        } 
        
        else {
            System.out.println("There are no duplicate elements in the array.");
        }
        
        sc.close();
    }    
}
