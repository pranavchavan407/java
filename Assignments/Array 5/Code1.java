/*1. WAP to check whether the array is in ascending order or not.

Input 1:
Enter the size of the array:4
Enter the elements of the array:
1
5
9
15

Output 1:
The given array is in ascending

Input 2 :

Enter the size of the array: 4
Enter the elements of the
1
15
9
4

Output 2 :
The given array is not ascending
*/
import java.util.*;
class Code1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        boolean assending = true;

        for(int i=0;i<size-1;i++){
            if(arr[i]>arr[i+1]){
                assending = false;
                break;
            }
        }

        if (assending) {
            System.out.println("The array is in ascending order.");
        }
        else{
            System.out.println("The array is not in ascending order.");
        }
        sc.close();
    }    
}
