/*
7. WAP to find the composite numbers in an array.

Input:
Enter the size of the array:6
Enter the elements of the array:
10
22
3
31
50
3

Output:
Composite numbers in an array are: 10,22,50
*/

import java.util.*;
class Code7{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the size of an array : ");

        int size = sc.nextInt();

        int arr[] = new int[size];

        for (int i = 0; i < arr.length; i++) {
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }

        
        System.out.print("Composite numbers in an array are: ");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]<=1) {
                continue;
            }
            else{
                for(int j=2;j<=arr[i]/2;j++){
                    if (arr[i]%j==0) {
                        System.out.print(arr[i]+" ");
                        break;
                    }
                }    
            }
        }
        sc.close();
    }
}