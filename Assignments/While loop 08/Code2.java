/*
 * Write a program to print the character sequence given below when the range given is
 
 input: start = 150, end = 198
 
 output: 151 153 155 157 159 161 163 165 167 169 171 173 175 177 179 181 183 185 187 189 191 193 195 197
 */

 class Code2 {
    public static void main(String[] args) {
        int num=150;
        while (num<=198) {
            if (num%2==1) {
                System.out.print(num+" ");
            }
            num++;
        }
    }
}
