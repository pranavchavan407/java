/*
Write a program to count the odd digits and even digits in the given number.

Input: 214367689

Output: Odd count : 4
        Even count : 5

*/
import java.util.*;
class Code9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Number: ");
        int num = sc.nextInt();
        sc.close();
        int oddNum = 0;
        int evenNum = 0;
        while (num>0) {
            int digit = num%10;
            if (digit%2==1) {
                oddNum++;
            }
            else{
                evenNum++;
            }
            num/=10;
        }
        System.out.println("Odd Count : "+oddNum);
        System.out.println("Even Count : "+evenNum);
    }
}
