/*
Write a program to print the odd digits of a given number.

Input : 216985
Output : 5 9 1

*/
import java.util.*;
class Code8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Number: ");
        int num = sc.nextInt();
        sc.close();
        while (num>0) {
            int digit = num%10;
            if (digit%2==1) {
                System.out.print(digit +" ");
            }
            num/=10;
        }
    }
}
