/*
Write a program to count the digits in the given number.

Input: num = 93079224
Output: Count of digits= 8

*/
import java.util.*;
class Code7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Number: ");
        int num = sc.nextInt();
        sc.close();
        int count=0;
        while (num>0) {
            count++;
            num/=10;
        }
        System.out.println("Count of digits= "+count);
    }
}
