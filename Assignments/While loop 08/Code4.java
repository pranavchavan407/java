/*
 * Write a program to print the character sequence given below when the range given is
 
 input: start = 1, end = 6
 
 Output: A 2 C 4 E 4

 */

 class Code4 {
    public static void main(String[] args) {
        int num=1;
        char ch = 65;
        while (num<=6) {
            if (num%2==0) {
                System.out.print(num+" ");
                ch++;
            }
            else{
                System.out.print(ch+" ");
                ch++;
            }
            num++;
        }
    }
}
