import java.util.*;
class InputDemo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter num : ");
		int num=sc.nextInt();

		if(num%16==0){
			System.out.println(num+" is in 16 table");
		}else{
			System.out.println(num+" is not in 16 table");
		}
	}
}
