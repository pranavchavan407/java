import java.util.*;
class MixedDemo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter no of rows : ");
		int row=sc.nextInt();

		for(int i=1; i<=row; i++){
			int num1=64+i;
			int num2=96+i;
			for(int sp=1; sp<=i-1; sp++){
				System.out.print("\t");
			}
			for(int j=1; j<=row-i+1; j++){
				if(row%2==0){
					System.out.print((char)num2++ +"\t");
				}else{
					System.out.print((char)num1++ +"\t");
				}
			}
			System.out.println();
		}
	}
}
			
