import java.util.*;
class Code2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        int sum = 0;
        int count = 0;

        for(int i=0;i<arr.length;i++){
            boolean prime = true;
            if (arr[i]<=1) {
                prime = false;
            }
            else{
                for(int j=2;j<=arr[i]/2;j++){
                    if (arr[i]%j==0) {
                        prime=false;
                        break;
                    }
                }
            }
            if (prime) {
                sum+=arr[i];
                count++;
            }
        }
        
        System.out.println("Sum of prime numbers: " + sum);
        System.out.println("Count of prime numbers: " + count);
        sc.close();
    }    
}
