import java.util.*;
class Code9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        int count = 0;

        for(int i=0;i<arr.length;i++){
            int reversedNum = 0;
            int originalNum = arr[i];
            
            while (arr[i]!=0){
                int digit = arr[i]%10;
                reversedNum = reversedNum*10+digit;
                arr[i]/=10;
            }

            if (originalNum==reversedNum) {
                count++;
            }

        }
        System.out.println("Count of palindrome numbers in the array is "+count);
        sc.close();
    }    
}
