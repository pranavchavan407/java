import java.util.*;
class Code3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        System.out.print("Enter Key: ");
        int num = sc.nextInt();

        int count = 0;

        for(int i=0;i<arr.length;i++){
            if (arr[i]==num) {
                count++;
            }    
        }
    
        if (count==0) {
            System.out.println("Key not found");
        }
        else if (count<3){
            System.out.println("Array will be same");
        }
        else {
            System.out.println("New array will be");
            for(int i=0;i<arr.length;i++){
                if (arr[i]==num) {
                    arr[i]=num*num*num;
                    System.out.print(arr[i]+" ");
                }
                else{
                    System.out.print(arr[i]+" ");
                }    
            }
        }
        sc.close();
    }    
}
