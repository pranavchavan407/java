import java.util.*;
class Code8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        char arr[] = new char[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.next().charAt(0);
        }
        
        System.out.println("Before reverse:");
        for(int i=0;i<arr.length;i+=2){
            System.out.print(arr[i]+" ");
        }
    
        int start = 0;
        int end = arr.length-1;

        while (start<end) {
            char temp = arr[start];
            arr[start] = arr[end];
            arr[end] = temp;
            start++;
            end--;
        }

        System.out.println();
        System.out.println("After reverse:");
        for(int i=0;i<arr.length;i+=2){
            System.out.print(arr[i]+" ");
        }
        sc.close();
    }    
}
