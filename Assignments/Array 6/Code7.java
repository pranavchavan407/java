import java.util.*;
class Code7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        for(int i=0;i<arr.length;i++){
            if (arr[i]<91 && arr[i]>64) {
                System.out.print((char)arr[i]+" ");
            }
            else{
                System.out.print(arr[i]+" ");
            }
        }
        sc.close();
    }    
}
