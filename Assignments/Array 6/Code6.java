import java.util.*;
class Code6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        System.out.print("Enter Key: ");
        int num = sc.nextInt();

        int count = 0;
        
        for(int i=0;i<size;i++){
            if (arr[i]%num==0) {
                System.out.println("An element multiple Of 5 found at index "+i);
                count++;
            }
        }

        if (count==0) {
            System.out.println("Element not found");
        }
        sc.close();
    }    
}
