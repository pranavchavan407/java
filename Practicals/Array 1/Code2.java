/*
2. Take an input from the user where the size of the array should be 10 and print the output of the user given elements of an array.
*/

import java.util.*;
class Code2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        System.out.print("Array Elements are : ");
        
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+",");
        }
        sc.close();
        
    }    
}
