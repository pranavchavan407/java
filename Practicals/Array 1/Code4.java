/*
4. Write a program to print the sum of odd elements in an array.Take input from the user.

Example:
Enter size: 10
Array:
1 2 3 4 2 5 6 2 8 10

Output :
Sum Of Odd elements : 9

*/
import java.util.*;
class Code4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        int sum = 0;
        for(int i=0;i<arr.length;i++){
            if (arr[i]%2==1) {
                sum+=arr[i];
            }    
        }

        System.out.println("Sum of Odd elements : "+sum);

        sc.close();
    }    
}
