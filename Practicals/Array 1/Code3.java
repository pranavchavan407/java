/*
3. Write a program to print the even elements in the array. Take input from the user.

Example:
Array:
10 11 12 13 14 15 16 17 18 19

Output :
10 
12
14
16
18

*/
import java.util.*;
class Code3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        for(int i=0;i<arr.length;i++){
            if (arr[i]%2==0) {
                System.out.println(arr[i]);
            }    
        }
        sc.close();
    }    
}
