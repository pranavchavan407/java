/*
1. Write a program to print the array with minimum 10 elements data.

Example:
Array:
10 20 30 40 50 60 70 80 90 100

Output :
Array Elements are : 10, 20, 30, 40, 50, 60, 70, 80, 90, 100
*/
class Code1 {
    public static void main(String[] args) {

        int arr[] = new int[10];
        arr[0]=10;
        arr[1]=20;
        arr[2]=30;
        arr[3]=40;
        arr[4]=50;
        arr[5]=60;
        arr[6]=70;
        arr[7]=80;
        arr[8]=90;
        arr[9]=100;

        System.out.print("Array Elements are : ");

        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+",");
        }
    }    
}
