/*
10. Write a real-time example where you have to use the array. Take input from the user.
*/

import java.util.*;
class Code10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the count of students present for the test : ");
        int count = sc.nextInt();
        int arr[] = new int[count];
        
        for(int i=0;i<arr.length;i++){
            System.out.print("Enter the marks of student "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }

        System.out.println("marks of students:");
        for(int i=0;i<arr.length;i++){
            System.out.println("Student "+(i + 1) + ": " + arr[i]);
        }
        sc.close();
    }   
}
