/*
6. Write a program where you have to take input from the user for a character array and
print the characters.

*/
import java.util.*;
class Code6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        char arr[] = new char[size];
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.next().charAt(0);
        }
        
        System.out.println("Array elements are : ");
        for(int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }    
        sc.close();    
    }
} 

