import java.util.*;
class Code4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        System.out.print("Enter the number to check: ");
        int num = sc.nextInt();

        int count = 0;

        for(int i=0;i<arr.length;i++){
            if (arr[i]==num) {
                count++;
            }    
        }
    
        if (count==0) {
            System.out.println(num+" does noot occur in an array");
        }
        else if (count<2){
            System.out.println(num+" occurs more than 1 times in an array");
        }
        else if (count==2) {
            System.out.println(num+" occurs 2 times in an array");
        }

        else {
            System.out.println(num+" occurs more than 2 times in an array");
        }
        sc.close();
    }    
}
