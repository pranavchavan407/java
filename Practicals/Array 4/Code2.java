import java.util.*;
class Code2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        int minNum = arr[0];
        int maxNum = arr[0];
        
        for(int i=0;i<arr.length;i++){
            if (minNum<arr[i]) {
                maxNum = arr[i];
            }
            else{
                minNum=arr[i];
            }    
        }
        
        System.out.print("The difference between the minimum and maximum elements is: "+(maxNum-minNum));

        sc.close();
    }    
}
