import java.util.*;
class Code8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        char arr[] = new char[size];
        
        System.out.println("Enter the array elements in UPPERCASE");
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.next().charAt(0);
        }
        
        System.out.print("Enter the character to check: ");
        char ch = sc.next().charAt(0);

        int count = 0;

        for(int i=0;i<arr.length;i++){

            if (arr[i]==ch) {
                count++;
            }    
        }
    
        if (count==0) {
            System.out.println(ch+" does noot occur in an array");
        }

        else {
            System.out.println(ch+" occurs "+count+ " times in an array");
        }
        sc.close();
    }    
}
