import java.util.*;
class Code9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        char arr[] = new char[size];
        

        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.next().charAt(0);
        }
        
        for(int i=0;i<arr.length;i++){
            if (arr[i]<97 || arr[i]>127) {
                arr[i]='#';
                System.out.println(arr[i]+" ");
            }

            else{
                System.out.println(arr[i]+" ");
            }
        }
        sc.close();
    }
}
