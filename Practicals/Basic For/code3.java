/**
 *  Write a program to print the first 10  3-digit numbers.
    
    Output: 101, 102, 103, 104, ...,109
 */
class code3 {
    public static void main(String[] args){
        for(int i=100;i<=109;i++){
            System.out.print(i + " ");
        }
    }   
}