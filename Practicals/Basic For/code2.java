/**
 *  Write a program to print the first 100 Whole numbers.
    
    Output: 0,1, 2, 3, 4, 5, ...,100
 */
class code2 {
    public static void main(String[] args){
        for(int i=0;i<=100;i++){
            System.out.print(i + " ");
        }
    }   
}