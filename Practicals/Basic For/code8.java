/**
 *  Write a program to print a table of 14 in reverse order from 140.
  Output: 140,126,112,..14
 */
class code8 {
    public static void main(String[] args){
        int x=14;
        for(int i=10;i>=1;i--){
            System.out.print(x*i + " ");
        }
    }   
}