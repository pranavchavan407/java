/**
 *  Write a program to print the odd numbers from 1 - 70.
 
    Output:1,3,5,7,...69
 */
class code5 {
    public static void main(String[] args){
        for(int i=1;i<=70;i=i+2){
            System.out.print(i + " ");
        }
    }   
}