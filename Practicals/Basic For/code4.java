/**
 *  Write a program to print the even numbers from 1 - 100.
    Output:2,4,6,8,10,...100
 */
class code4 {
    public static void main(String[] args){
        for(int i=2;i<=100;i=i+2){
            System.out.print(i + " ");
        }
    }   
}