/*
    Q3 Write a program to print the following pattern.
    
    Number of rows = 3
    1 1 1
    2 2 2
    3 3 3

    Number of rows = 4
    1 1 1 1
    2 2 2 2
    3 3 3 3
    4 4 4 4
*/

import java.util.*;
class code3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows : ");
        int rows = sc.nextInt();
        for(int i=1;i<=rows;i++){
            for(int j=1;j<=rows;j++){
                System.out.print(i + " ");
            }
            System.out.println();
        }
        sc.close();
    }  
}