/*
    Q2 Write a program to print the following pattern.
    
    Number of rows = 3
    1 2 3
    1 2 3
    1 2 3

    Number of rows = 4
    1 2 3 4
    1 2 3 4
    1 2 3 4
    1 2 3 4
*/

import java.util.*;
class code2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows : ");
        int rows = sc.nextInt();
        for(int i=1;i<=rows;i++){
            for(int j=1;j<=rows;j++){
                System.out.print(j + " ");
            }
            System.out.println();
        }
        sc.close();
    }  
}