/*
    Q6 Write a program to print the following pattern

    Number of rows = 3
    1A 2B 3C
    1A 2B 3C
    1A 2B 3C

    Number of rows = 4
    1A 2B 3C 4D
    1A 2B 3C 4D
    1A 2B 3C 4D
    1A 2B 3C 4D

*/


import java.util.*;
class code6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows : ");
        int rows = sc.nextInt();
        for(int i=1;i<=rows;i++){
            char ch = 'A';
            for(int j=1;j<=rows;j++){
                System.out.print(j+""+ch+" ");
                ch++;
            }
            System.out.println();
        }
        sc.close();
    }  
}