/*
  Q7 Write a program to print the following pattern
    
    Number of rows = 4
    d c b a
    d c b a
    d c b a
    d c b a

    Number of rows = 3
    c b a
    c b a
    c b a

 */

 import java.util.*;
 class code7 {
    public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows : ");
        int rows = sc.nextInt();
        for(int i=1;i<=rows;i++){
          char ch = (char)(96+rows);
          for(int j=1;j<=rows;j++){
            System.out.print(ch+" ");
            ch--;
          }
          System.out.println();
        }
        sc.close();
    }  
 }