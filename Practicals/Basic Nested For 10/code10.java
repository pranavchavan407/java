/*
Q10 Write a program to print the following pattern

Number of rows = 4
1 2 3 4
2 3 4 5
3 4 5 6
4 5 6 7

Number of rows = 3
1 2 3
2 3 4
3 4 5

*/

import java.util.*;
class code10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the number of rows : ");
        int rows = sc.nextInt();
        for(int i=0;i<rows;i++){
            for(int j=0;j<rows;j++){
                System.out.print(i+j+1+" "); 
            }
            System.out.println();
        }
        sc.close();
    }  
}