class WhileDemo{
	public static void main(String[]args){
		int num=214367689;
		int rem=0;
		int Ecnt=0;
		int Ocnt=0;

		while(num>0){
			rem=num%10;
			if(rem%2==0){
				Ecnt++;
			}else{
				Ocnt++;
			}
			num=num/10;
		}
		System.out.println("Even number count is"+ Ecnt);
		System.out.println("Odd number count is"+ Ocnt);
	}
}
