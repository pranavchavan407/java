/*
7.WAP to print the array , if the user given size of an array is even then print the altemate elements in an array, else print the whole array.

Example 1:

Input:
Enter the size : 5
Enter elements:
1
2
3
4
5

Output:
Array elements are: 1 2 3 4 5

Example 2:

Input:
Enter the size : 4
Enter elements:
1
2
3
4

Output:
Array elements are: 1 3

*/

import java.util.*;
class Code7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        if(size%2==0){
            System.out.print("Array Elements are : ");
        
            for(int i=0;i<arr.length;i++){
                System.out.print(arr[i]+",");
                i++;
            }
        }

        else{
            System.out.print("Array Elements are : ");
        
            for(int i=0;i<arr.length;i++){
                System.out.print(arr[i]+",");
            }
        }
        sc.close();
        
    }    
}
