/*
3.WAP to check if there is any vowel in the array of characters if present then print its index, where you have to take the size and elements from the user.

Example:
Enter size: 5
Enter elements: 
a
r
E
K
O

Output:

vowel a found at index O
vowel E found at index 2
vowel O found at index 4

*/
import java.util.*;
class Code3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        char arr[] = new char[size];
        
        System.out.println("Enter Elements : ");

        for(int i=0;i<size;i++){
            System.out.print("");
            arr[i] = sc.next().charAt(0);
        }
        
        for(int i=0;i<arr.length;i++){
            char ch =Character.toLowerCase(arr[i]);
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u'){

                System.out.println("Vowel "+arr[i]+" found at index "+i);
            } 
        }
        sc.close();
    }    
}
