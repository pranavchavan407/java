/*
1. Write a program to count  the even numbers in the array where you have to take the size and elements from the user. And also print the even numbers too.

Example:
Enter size: 8
Array:
1 12 55 65 44 22 36 10

Output :
even numbers 12 44 22 36 10
Count of even elements is : 5

*/
import java.util.*;
class Code1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        
        for(int i=0;i<size;i++){
            System.out.print("Enter the Element "+(i+1)+": ");
            arr[i] = sc.nextInt();
        }
        
        int count = 0;
        System.out.print("Even Numbers : ");
        
        for(int i=0;i<arr.length;i++){
            if (arr[i] % 2 == 0) {
                System.out.print(arr[i]+" ");
                count++;
            }    
        }
        System.out.println();
        
        System.out.print("Count of Even Numbers : "+count);

        sc.close();
    }    
}
