/*
10. WAP to print the maximum element in the array, where you have to take the size and elements from the user.

Example:

Input:

Enter the array size: 5
Enter Elements 1: 7
Enter Elements 2: 81
Enter Elements 3: 65
Enter Elements 4: 12
Enter Elements 5: 23

Output:
Maximun nuber in the array is found at pos 1 is 81

*/

import java.util.*;

class Code10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        
        int arr[] = new int[size];
        for(int i=0;i<arr.length;i++){
            System.out.print("Enter Elements "+(i+1)+": ");
            arr[i]=sc.nextInt();
        }
        int maxNum = arr[0];
        int position = 0;

        for(int i=0;i<arr.length;i++){
            if (arr[i] > maxNum){
                maxNum = arr[i];
                position = i;
            }
        }

        System.out.println("Maximun nuber in the array is found at pos "+position+" is "+maxNum);

        sc.close();
    }
}