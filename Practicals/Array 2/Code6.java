/*
6.Write a program to print the products of odd indexed elements in an array. Where you have to take size input and elements input from the user.

Example:

Input:
Enter the size : 6
Enter elements:
1
2
3
4
5
6

Output:
Product of odd indexed elements : 48

*/

import java.util.*;
class Code6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the array size: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        
        System.out.println("Enter elements");

        for(int i=0;i<size;i++){
            arr[i] = sc.nextInt();
        }
        
        int product = 1;

        for(int i=0;i<arr.length;i++){
            if (i%2==1){
                product*=arr[i];
            }
        }
        System.out.println("Product of odd indexed elements: "+product);
        sc.close();
    }    
}
