/*
    Write a program to print the sum of odd numbers from 150 to 101.
    Output : 3125
*/

class code9 {
    public static void main(String[] args) {
        int x=150;
        int sum=0;
        while (x>=101) {
            if (x%2==1) {
                sum=sum+x;
            }
            x--;
        }
        System.out.println(sum);
    }
}
