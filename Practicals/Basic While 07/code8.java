/*
    Write a program where you have to print sum of integer from 90 to 11
    Output : 4040
*/

class code8 {
    public static void main(String[] args) {
        int x=90;
        int sum =0;
        while (x>=11) {
            sum=sum+x;
            x--;
        }
        System.out.println(sum);
    }
}
