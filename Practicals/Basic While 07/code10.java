/*
    Write a program to print the numbers in the range 100 - 24 which are divisible by
    4 & 5.
    Output: 100,80,60,40
*/

class code10 {
    public static void main(String[] args) {
        int x=100;
        while (x>=24) {
            if (x%4==0 && x%5==0) {
                System.out.println(x);
            }
            x--;
        }
    }
}
