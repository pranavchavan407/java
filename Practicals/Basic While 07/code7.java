/*
    Write a program to print the cube of the first 10 natural numbers.
    Output : 1 8 27 64 125 216 343 512 729 1000
*/

class code7 {
    public static void main(String[] args) {
        int x=1;
        while (x<=10) {
            System.out.print(x*x*x + " ");
            x++;
        }
    }
}
