/*
 * Write a program to print the numbers divisible by 5 in the range 50 - 1O.
 *  */

 class code2 {
    public static void main(String[] args) {
        int x =50;
        while (x>=10) {
            if (x%5==0) {
                System.out.println(x);
            }
            x--;
        }
    }
}
